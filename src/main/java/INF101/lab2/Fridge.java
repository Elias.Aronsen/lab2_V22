package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
	
	int max_size = 20;
	List<FridgeItem> listOfItems;
	
	
	public Fridge() {
		
		listOfItems = new ArrayList<FridgeItem>();
		}
	
	public static void main(String[] args) {
		
		System.out.println("Hello");
		
	}
	
	
	@Override
	public int nItemsInFridge() {

		return listOfItems.size();
	}

	@Override
	public int totalSize() {

		return max_size;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		
			if (listOfItems.size() >= max_size) {
				return false;
				}
			
			listOfItems.add(item);
			return true;
	}

	@Override
	public void takeOut(FridgeItem item) {
		
		if(!listOfItems.contains(item)) {
			throw new NoSuchElementException("No such item!");	
		}
		listOfItems.remove(item);
	}

	@Override
	public void emptyFridge() {

		listOfItems.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		/**
		 * Remove all items that have expired from the fridge
		 * @return a list of all expired items
		 */
		ArrayList<FridgeItem> expiredFood = new ArrayList<>();
		
		for(int i = 0; i < nItemsInFridge(); i++) {
			
			FridgeItem item = listOfItems.get(i);
			
			if(item.hasExpired()) {
				expiredFood.add(item);
			}}
		
		for(FridgeItem expiredItem : expiredFood) {
			listOfItems.remove(expiredItem);
		} 
		return expiredFood;
	}

}
